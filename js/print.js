(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.print = {
        attach: function (context, settings) {
            $('[data-share="print"]', context)
                .on('click', function() {
                    window.print();
                });
        }
    };

})(jQuery, Drupal, window);