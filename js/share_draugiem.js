(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.shareDraugiem = {
        attach: function (context, settings) {
            var label, url;

            label = settings.socialShare.label;
            url = settings.socialShare.url;

            $('[data-share="draugiem"]', context)
                .on('click', function() {
                    window.open(
                        'http://www.draugiem.lv/say/ext/add.php?' +
                        'title=' + encodeURIComponent(label) +
                        '&link=' + encodeURIComponent(url),
                        '_blank', 'width=530,height=400,resizable=1'
                    );
                });
        }
    };

})(jQuery, Drupal, window);