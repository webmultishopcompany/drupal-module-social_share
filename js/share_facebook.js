(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.shareFacebook = {
        attach: function (context, settings) {
            var url;

            url = settings.socialShare.url;

            $('[data-share="facebook"]', context)
                .on('click', function() {
                    window.open(
                        'https://www.facebook.com/sharer/sharer.php?' +
                        'u=' + encodeURIComponent(url),
                        '_blank', 'width=555,height=419,resizable=1'
                    );
                });
        }
    };

})(jQuery, Drupal, window);