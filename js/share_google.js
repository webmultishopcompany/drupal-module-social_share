(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.shareGoogle = {
        attach: function (context, settings) {
            var url;

            url = settings.socialShare.url;

            $('[data-share="google"]', context)
                .on('click', function() {
                    window.open(
                        'https://plus.google.com/share?' +
                        'url=' + encodeURIComponent(url),
                        '_blank', 'width=400,height=430,resizable=1'
                    );
                });
        }
    };

})(jQuery, Drupal, window);