(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.sharePinterest = {
        attach: function (context, settings) {
            var url, image, label;

            url = settings.socialShare.url;
            label = settings.socialShare.label;
            image = settings.socialShare.image;

            $('[data-share="pinterest"]', context)
                .on('click', function() {
                    window.open(
                        'https://pinterest.com/pin/create/button/?' +
                        'url=' + encodeURIComponent(url) +
                        '&media=' + encodeURIComponent(image) +
                        '&description=' + encodeURIComponent(label),
                        '_blank', 'width=530,height=400,resizable=1'
                    );
                });
        }
    };

})(jQuery, Drupal, window);