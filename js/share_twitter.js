(function($, Drupal, window) {

    'use strict';

    Drupal.behaviors.shareTwitter = {
        attach: function (context, settings) {
            var label, url;

            label = settings.socialShare.label;
            url = settings.socialShare.url;

            $('[data-share="twitter"]', context)
                .on('click', function() {
                    window.open(
                        'https://twitter.com/intent/tweet?' +
                        'text=' + encodeURIComponent(label + '\n') + encodeURIComponent(url),
                        '_blank', 'width=530,height=253,resizable=1'
                    );
                });
        }
    };

})(jQuery, Drupal, window);