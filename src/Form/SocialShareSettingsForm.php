<?php

namespace Drupal\wmc_social_share\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure file system settings for this site.
 */
class SocialShareSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * @var \Drupal\node\NodeTypeInterface[]
   */
  protected $node_types;

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->entity_type_manager = $entity_type_manager;
    $this->node_types = $this->entity_type_manager->getStorage('node_type')->loadMultiple();
    $this->settings = $this->config('wmc_social_share.settings');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wmc_social_share_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['wmc_social_share.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['enabled_types'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Enabled entity types'),
      '#description' => $this->t('Enable to add social share links for the given type.'),
      '#tree' => TRUE,
    ];

    foreach ($this->node_types as $node_type) {
      $form['enabled_types'][$node_type->id()] = [
        '#type' => 'checkbox',
        '#title' => $node_type->label(),
        '#default_value' => $node_type->getThirdPartySetting('wmc_social_share', 'enabled', FALSE),
      ];
    }

    $form['enabled_sharing_types'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Enabled sharing types'),
      '#description' => $this->t('Changing this field will invalidate cache for all nodes.'),
      '#tree' => TRUE,
    ];

    /** @var NodeType $node_type */
    $enabled_sharing_types = $this->settings->get('enabled_sharing_types');
    foreach ($enabled_sharing_types as $key => $value) {
      $form['enabled_sharing_types'][$key] = [
        '#type' => 'checkbox',
        '#title' => $key,
        '#default_value' => $value,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $enabled_types = $form_state->getValue('enabled_types');
    foreach ($enabled_types as $type => $enabled) {
      $node_type = $this->node_types[$type];
      if ($node_type->getThirdPartySetting('wmc_social_share', 'enabled', FALSE) != (bool) $enabled) {
        $node_type->setThirdPartySetting('wmc_social_share', 'enabled', (bool) $enabled);
        $node_type->save();
      }
    }

    $sharing_types = $form_state->getValue('enabled_sharing_types');
    if ($this->settings->get('enabled_sharing_types') != $sharing_types) {
      $this->settings->set('enabled_sharing_types', $sharing_types);
      $this->settings->save();
      Cache::invalidateTags(['node_list']);
    }

    parent::submitForm($form, $form_state);
  }

}
